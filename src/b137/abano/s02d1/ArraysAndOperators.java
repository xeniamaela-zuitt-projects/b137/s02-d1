package b137.abano.s02d1;

public class ArraysAndOperators {
    public static void main (String[] args) {
        System.out.println("Arrays and Operators\n");

        //declare an array of integers
        int[] arrayOfNumbers = new int[5];

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        System.out.println();

        //mini activity - manual initialization
        arrayOfNumbers[0] = 30;
        arrayOfNumbers[1] = 11;
        arrayOfNumbers[2] = 23;
        arrayOfNumbers[3] = 35;
        arrayOfNumbers[4] = 0;

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        System.out.println();

        //declare an array of names
        String[] arrayOfNames = {"Curry", "Thompson", "Green", "Durant", "Irving"};
        System.out.println("The name at index 4 is: " + arrayOfNames[4]);

        //System.out.println();

        //declare an array od arbitrary
        //int[] arrayOfArbitraryNumbers;
        //ArrayLists is the solution
    }
}
